#include <boost/python.hpp>


#include <trillBPP/vector/vec.h>
#include <trillBPP/BPSK/BPSK.h>
#include <trillBPP/comm/reedsolomon.h>
#include <string>
#include <iostream>


using namespace std;
using namespace trill;

template <typename T>
std::string to_str(const T &i)
{
  std::ostringstream ss;
  ss.precision(8);
  ss.setf(std::ostringstream::scientific, std::ostringstream::floatfield);
  ss << i;
  return ss.str();
}

void format(string &val) {
	replace( val.begin(), val.end(), ' ', ',');
	replace( val.begin(), val.end(), '[', ' ');
	replace( val.begin(), val.end(), ']', ' ');
}

//------Other

string trillDecoder(string &input) 
{
	BPSK bP;

	string val = to_str(bP.trillDecoder(input));
	format(val);
	return val;
}

string reed_encode(string input)	//Length of input should be k*m, i.e, 33*6=198 bits
{
	  int m, t, n, k, q, NumBits, NumCodeWords;
	  double p;
	  bvec uncoded_bits= input;
	  bvec coded_bits, received_bits, decoded_bits;
	  //Set parameters:
	  NumCodeWords = 1000;  //Number of Reed-Solomon code-words to simulate
	  p = 0.01;             //BSC Error probability
	  m = 6;               //Reed-Solomon parameter m
	  t = 15;                //Reed-Solomon parameter t

	  //Classes:
	  Reed_Solomon reed_solomon(m, t);
	  //Calculate parameters for the Reed-Solomon Code:
	  n = static_cast<int>(pow(2.0, m) - 1 );
	  k = static_cast<int>(pow(2.0, m) ) - 1 - 2 * t;
	  q = static_cast<int>(pow(2.0, m) );
	  coded_bits = reed_solomon.encode(uncoded_bits);
		
	string final =to_str(coded_bits);
	format(final);
	return final;
}

string reed_decode(string input)
{	int m=6, t=15;	
	Reed_Solomon reed_solomon(m, t);
	string decoded_bits = to_str(reed_solomon.decode(input));
	format(decoded_bits);
	return decoded_bits;
}


BOOST_PYTHON_MODULE(bpsk)
{
    using namespace boost::python;
    def("trillDecoder", trillDecoder); 
    def("reed_decode", reed_decode);
    def("reed_encode", reed_decode);
}
