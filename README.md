# README #

### What is this repository for? ###

This repository is for creating cross-platform shared libraries for Trillbit with FFTW included. 

### How do I get set up? ###

There are two scripts to execute:

1. fftw_lib_gen.sh

2. trill_lib_gen.sh

Before running either of the scripts, please follow the following guidelines:

1. chmod +x <script_name>  [ To make it executable ]

2. update bash to >=4 in your system and update the first line in the scripts 

#### For FFTW static library (libfftw3.a) generation: ####

```export NDK_ROOT="/Users/rajanya/Library/Android/sdk/ndk-bundle" && ./fftw_lib_gen.sh```

After running this, the fftw3.a files will directly be moved into ../lib/${architecture_name}

#### For Trill shared library (libtrillBPP.so) generation: ####

```export NDK_ROOT="/Users/rajanya/Library/Android/sdk/ndk-bundle" && ./trill_lib_gen.sh```


This CMake and toolchain runs for Android NDK, Revision 15c
If you have toolchain error coming while compiling the library please download Android NDK, Revision 15c 
Download Link : https://developer.android.com/ndk/downloads/older_releases



------------------------------
Error detected when executing Things i am trying point wise : 
1. Try - 1 : removed --disable fortran, and after the try enabled again.
2. Try - 2 : put fftw_execute_dft instead of fftw_execute
3. Try - 3 : removed '&' from trillDecoder_RLS in function call.
4. Try - 4 : destory plan before creating a new plan
5. Try - 5 : fftw_plan p1(NULL), here NULL removed.
6. Try - 6 : fftw_malloc, and creating plan before initializing
