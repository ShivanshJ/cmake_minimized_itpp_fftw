/*
 * modulation.h
 *
 *  Created on: 29-Aug-2017
 *      Author: shivujagga
 */

#ifndef _MODULATION_H_
#define _MODULATION_H_

#include <fftw3.h>
#include <android/log.h>

#include <trillBPP/vector/transforms.h>
#include <trillBPP/miscfunc.h>		//For xcorr - levels2bit,cvec, conj
#include <trillBPP/vector/binary.h> //For xcorr - abs


using namespace std;
using namespace trill;


class modulation {
private:
	void xcorr(const cvec &x, const cvec &y, cvec &out, const int max_lag = -1, const std::string scaleopt = "none",
	           bool autoflag = true);
public:
//@convolve_time_domain -> Normal COnvolution with O(n^2)complexity
//@convolve_freq_domain -> FFT COnvolution with O(nLogn)complexity
	vec convolve_time_domain(vec data_bits, vec rrc_wave);
	vec convolve_freq_domain(vec &data_bits, vec &rrc_wave);
	cvec convolve_freq_domain(const cvec &data_bits, vec &rrc_wave);

//Overloaded functions- Auto correlate and Cross-correlate
	vec xcorr(const vec &x, const vec &y, const int max_lag = -1, const std::string scaleopt = "none");
	vec xcorr(const vec &x, const int max_lag = -1, const std::string scaleopt = "none");


//FFT
//	cvec fft(cvec input);

};



#endif /* MODULATION_H_ */
