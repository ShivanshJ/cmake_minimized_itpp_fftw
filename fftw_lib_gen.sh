

while [[ $# -gt 1 ]]
do
	key="$1"

	case $key in
		--fftwdir)
                FFTW_DIR="$2"
                shift # past argument
                ;;
    		--ndkroot)
    		NDK_ROOT="$2"
    		shift # past argument
    		;;
		--androidapi)
                ANDROID_API_LEVEL="$2"
                shift # past argument
                ;;
    		*)
            		# unknown option
    		;;
	esac
	shift # past argument or value
done
	
if [ -z "$NDK_ROOT" ]
then
	echo " <ERROR> Please run the command: $ ./trill_lib_gen.sh --ndkroot <path_to_ndk_root> [Eg: $HOME/Android/Sdk/ndk-bundle]"
	exit
fi

if [ -z "$FFTW_DIR" ]
then
      echo " <WARN> Please provide fftw's directory (optional): $ ./trill_lib_gen.sh --ndkroot <path_to_ndk_root> --fftwdir <path_to_fftw_dir> "
      echo " ****** Setting FFTW_DIR=fftw-3.3.6-pl2 ******"
      FFTW_DIR=fftw-3.3.6-pl2
fi

if [ -z "$ANDROID_API_LEVEL" ]
then
      echo " <WARN> Please provide android API level (optional): $ ./trill_lib_gen.sh --ndkroot <path_to_ndk_root> --androidapi <api_level>"
      echo " ****** Setting ANDROID_API_LEVEL=21 ******"
      ANDROID_API_LEVEL=21
fi

declare -A abi_toolchain_map
abi_toolchain_map["armeabi"]="arm-linux-androideabi" 
abi_toolchain_map["arm64-v8a"]="aarch64-linux-android" 
abi_toolchain_map["x86"]="x86" 
abi_toolchain_map["x86_64"]="x86_64" 
abi_toolchain_map["mips"]="mipsel-linux-android" 
abi_toolchain_map["mips64"]="mips64el-linux-android"

declare -A abi_toolbin_map
abi_toolbin_map["armeabi"]="arm-linux-androideabi"
abi_toolbin_map["arm64-v8a"]="aarch64-linux-android"
abi_toolbin_map["x86"]="i686-linux-android"                
abi_toolbin_map["x86_64"]="x86_64-linux-android"              
abi_toolbin_map["mips"]="mipsel-linux-android"
abi_toolbin_map["mips64"]="mips64el-linux-android"

declare -A abi_arch_map
abi_arch_map["armeabi"]="arm"
abi_arch_map["arm64-v8a"]="arm64"
abi_arch_map["x86"]="x86"
abi_arch_map["x86_64"]="x86_64"
abi_arch_map["mips"]="mips"
abi_arch_map["mips64"]="mips64"


echo "...... Entering fftw directory ......"
cd $FFTW_DIR
echo "..... Present working directory: $PWD ......"

echo "-making directory build which will store static file temporarily"
mkdir build

for abi in "${!abi_toolchain_map[@]}"
do
	echo "############## FFTW static library generation for : $abi - ${abi_toolchain_map[$abi]} ###########"

	export PATH="$NDK_ROOT/toolchains/${abi_toolchain_map[$abi]}-4.9/prebuilt/linux-x86_64/bin:$PATH"
	export SYS_ROOT="$NDK_ROOT/platforms/android-$ANDROID_API_LEVEL/arch-${abi_arch_map[$abi]}/"
	export CC="${abi_toolbin_map[$abi]}-gcc --sysroot=$SYS_ROOT"
	export LD="${abi_toolbin_map[$abi]}-ld"
	export AR="${abi_toolbin_map[$abi]}-ar"
	export RANLIB="${abi_toolbin_map[$abi]}-ranlib"
	export STRIP="${abi_toolbin_map[$abi]}-strip"
	
	
	DEST_DIR="`pwd`/build/$abi"
	cd build
	mkdir -p $abi
	cd ..

        ./configure --disable-fortran --host=${abi_toolbin_map[$abi]} --prefix=$DEST_DIR LIBS="-lc -lgcc" 
	make && make install

	echo "...... Copying libfftw3.a to lib ......."
	mkdir -p ../lib/$abi
	mv $DEST_DIR/lib/*.a ../lib/$abi/
	#DEST_DIR/lib because lib folder is premade along with 3 more folders. And lib folder has the .so and .a libraries
done

cd ..
