# Add this without quotes and then magic '#!/usr/local/Cellar/bash/4.4.12/bin/bash'

while [[ $# -gt 1 ]]
do
	key="$1"

	case $key in
    		--ndkroot)
    		NDK_ROOT="$2"
    		shift # past argument
    		;;
    		--toolchain)
    		TOOLCHAIN="$2"
    		shift # past argument
    		;;
    		*)
            		# unknown option
    		;;
	esac
	shift # past argument or value
done

if [ -z "$NDK_ROOT" ]
then
	echo " <ERROR> Please run the command: $ ./trill_lib_gen.sh --ndkroot <path_to_ndk_root> [Eg: $HOME/Android/Sdk/ndk-bundle]"
	exit
fi

if [ -z "$TOOLCHAIN" ]
then
      echo " <WARN> Please provide toolchain (optional): $ ./trill_lib_gen.sh --ndkroot <path_to_ndk_root> --toolchain <path_to_toolchain>"
      TOOLCHAIN=android.toolchain.cmake 
fi

declare -A abi_toolchain_map
abi_toolchain_map["armeabi"]="arm-linux-androideabi" 
abi_toolchain_map["arm64-v8a"]="aarch64-linux-android" 
abi_toolchain_map["x86"]="x86" 
abi_toolchain_map["x86_64"]="x86_64" 
abi_toolchain_map["mips"]="mipsel-linux-android" 
abi_toolchain_map["mips64"]="mips64el-linux-android"

declare -A abi_toolbin_map
abi_toolbin_map["armeabi"]="arm-linux-androideabi"
abi_toolbin_map["arm64-v8a"]="aarch64-linux-android"
abi_toolbin_map["x86"]="i686-linux-android"                
abi_toolbin_map["x86_64"]="x86_64-linux-android"              
abi_toolbin_map["mips"]="mipsel-linux-android"
abi_toolbin_map["mips64"]="mips64el-linux-android"

for abi in "${!abi_toolchain_map[@]}"
do
	echo "........ Trill shared library generation for : $abi - ${abi_toolchain_map[$abi]} ........."

	echo "........ Checking libfftw3.a in lib/$abi to begin with ......."
	if [ ! -f lib/$abi/libfftw3.a ]; then
		echo "<ERROR>..... FFTW library not found! Please run fftw_lib_gen.sh ......"
		continue
	fi

	mkdir build && cd build
	
	
	cmake .. -DANDROID_NATIVE_API_LEVEL=17 -DCMAKE_TOOLCHAIN_FILE=$TOOLCHAIN -DANDROID_NDK=$NDK_ROOT -DANDROID_TOOLCHAIN_NAME=${abi_toolchain_map[$abi]}-4.9 -DANDROID_ABI=$abi

	make
	make install

	echo "...... Copying .so to lib ......."
	cd ..
	mkdir -p lib/$abi
	mv build/*.so lib/$abi/

	#Running strip command by going inside the folder
	cd lib/$abi/
	$NDK_ROOT/toolchains/${abi_toolchain_map[$abi]}-4.9/prebuilt/linux-x86_64/bin/${abi_toolbin_map[$abi]}-strip -g -S -d --strip-debug --strip-unneeded --strip-all libtrillBPP.so
	cd ..
	cd ..

	echo "...... Removing build ......"
	rm -rf build

done
