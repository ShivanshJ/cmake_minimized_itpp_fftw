#Cross Compile FFTW for Android Architectures. Step by step:
#Step 1 -> A few folder names have to be changed in /../Android/Sdk/ndk-bundle/toolchains
#Change: 'x86-4.9' to 'i686-linux-android-4.9'
#Change: 'x86_64' to 'x86_64-linux-android'
#Run each of them to get the named architecture
#
#Step 2->If the made libraries and headers have to be used in another library being built(like ITPP). Then along with the commands in terminal,
#	$mkdir build && cd build
#	$cmake ..
#	$make && make install
#Write the following for each library:
#	$mkdir build && cd build
#	$cmake .. -DANDROID_NATIVE_API_LEVEL=17 -DCMAKE_TOOLCHAIN_FILE=/home/shivujagga/Softwares/CMakers/android.toolchain.cmake -DANDROID_NDK=/home/shivujagga/Android/Sdk/ndk-bundle -DFFT_INCLUDES=/home/shivujagga/Downloads/Packages/fftw-3.3.6-pl2/build/include -DFFT_LIBRARIES=/home/shivujagga/Downloads/Packages/fftw-3.3.6-pl2/build/lib/libfftw3.so -DANDROID_ABI=x86
#	$make && make install
#
#
#Step 3->Note : ANDROID_ABI=x86 has to be changed for each architecture while building, i.e, x86_64,mips,mips64,armeabi,arm64-v8a


INSTALL_DIR="`pwd`/build"
SRC_DIR="`pwd`"
cd $SRC_DIR
mkdir -p $INSTALL_DIR

declare -a archs
archs=(arm arm64 x86 x86_64 mips mips_64)

declare -a arch_toolbin_folder
arch_toolbin_folder=(arm-linux-androideabi aarch64-linux-android x86 x86_64 mipsel-linux-android mips64el-linux-android)

declare -a arch_name
arch_name=(arm-linux-androideabi aarch64-linux-android i686-linux-android x86_64-linux-android mipsel-linux-android mips64el-linux-android)

declare -a arch_sys_dir
arch_sys_dir=(arch-arm arch-arm64 arch-x86 arch-x86_64 arch-mips arch-mips64)

#Note : Confirm the directory names, example: In Mac, it is Android/sdk and Linux is Android/Sdk. Small difference in 'S' Capital.
#ARCH_NAMES are as follows
#1. arm-linux-androideabi : For armv7a and armeabi
#2. aarch64-linux-android : For arm64-v8a
#3. i686-linux-android :  For x86 architecture
#4. x86_64-linux-android : For x86 - 64 bit
#5. mipsel-linux-android
#6. mips64el-linux-android

for n in "${!archs[@]}"
do
export ARCH_NAME="${arch_name[$n]}"
export NDK_ROOT="/home/shivujagga/Android/Sdk/ndk-bundle" # <------------------------Change this
export PATH="$NDK_ROOT/toolchains/${arch_toolbin_folder[$n]}-4.9/prebuilt/linux-x86_64/bin/:$PATH"
#IMPORTANT : 
#1. uses: arch-arm
#2. uses: arch-arm64
#3. uses: arch-x86
#4. uses: arch-x86_64
#5. uses: arch-mips
#6. uses: arch-mips64
export SYS_ROOT="$NDK_ROOT/platforms/android-21/${arch_sys_dir[$n]}/"
export CC="$ARCH_NAME-gcc --sysroot=$SYS_ROOT"
export LD="$ARCH_NAME-ld"
export AR="$ARCH_NAME-ar"
export RANLIB="$ARCH_NAME-ranlib"
export STRIP="$ARCH_NAME-strip"

NEW_DIR="`pwd`/build/${archs[$n]}"
cd $INSTALL_DIR
mkdir -p ${archs[$n]}
cd $SRC_DIR

./configure --host=$ARCH_NAME --disable-fortran --enable-shared --prefix=$NEW_DIR LIBS="-lc -lgcc"
make
make install
echo "after this"
echo $ARCH_NAME, "${arch_sys_dir[$n]}"


done
